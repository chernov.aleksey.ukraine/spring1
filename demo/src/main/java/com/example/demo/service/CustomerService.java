package com.example.demo.service;

import com.example.demo.domain.Account;
import com.example.demo.domain.Currency;
import com.example.demo.domain.Customer;
import java.util.List;
public interface CustomerService {
    List<Customer> getAll();
    Customer getById(Long userId);
    void delete(long id );
    boolean update(Customer customer);
    void create(Customer employee);
    void createCustomerAccount (Long customerId, Currency currency);
    boolean deleteCustomerAccount (Long customerId, Long accountId);
}

package com.example.demo.service;
import com.example.demo.domain.Account;
import com.example.demo.domain.Customer;
import java.util.List;
public interface AccountService {
    List<Account> getAll();
    Account getById(Long userId);
    void delete(long id );
    void create(Account employee);
    void topUpAccount(String accountNumber, Double amount);
    boolean withdrawMoney(String accountNumber, Double amount);
    boolean transferMoney(String numberAccountFrom, Double amount, String numberAccountTo);
}

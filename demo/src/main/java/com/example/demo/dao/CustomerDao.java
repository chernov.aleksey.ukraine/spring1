package com.example.demo.dao;

import com.example.demo.domain.Customer;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDao implements Dao<Customer>{
    List<Customer> customers = new ArrayList<>();
    @PostConstruct
    public void init(){
        customers.add(new Customer( "Ivan Petrenko", "ivanpetrenko@mail.com", 22, new ArrayList<>()));
        customers.add(new Customer( "Oksana Petrenko", "oksanapetrenko@mail.com", 21, new ArrayList<>()));
        customers.add(new Customer( "Petro Ivanenko", "petroivanenko@mail.com", 23, new ArrayList<>()));
        customers.add(new Customer( "Natalya Ivanenko", "natalyaivanenko@mail.com", 22, new ArrayList<>()));
        customers.add(new Customer( "Sergiy Stepanenko", "sergiystepanenko@mail.com", 24, new ArrayList<>()));
        customers.add(new Customer( "Galyna Stepanenko", "galynastepanenko@mail.com", 23, new ArrayList<>()));
       }
    @Override
    public Customer save(Customer obj) {
        customers.add(obj);
        return obj;}
    @Override
    public boolean delete(Customer obj) {
        if (customers.contains(obj)) {

            customers.remove(obj);
            System.out.println(customers);
            return true;}
        return false;}
    @Override
    public void deleteAll(List<Customer> entities) {
        customers.removeAll(entities);
    }
    @Override
    public void saveAll(List<Customer> entities) {
        customers.addAll(entities);
    }
    @Override
    public List<Customer> findAll() {
        return customers;
    }
    @Override
    public boolean deleteById(long id) {customers.forEach(e -> {if (e.getId().equals(id)) {customers.remove(e);}});
        return true;}
    @Override
    public Customer getOne(long id) {return customers.stream().filter(e -> e.getId().equals(id)).findFirst().get();}
}
